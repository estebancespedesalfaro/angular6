import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';

export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje>= new BehaviorSubject<DestinoViaje>(null); 
	//objeto observable current, para que avise cuando se ha seleccionado un nuevo
	//destino como favorito

	constructor() {
       this.destinos = [];
	}
	add(d: DestinoViaje){
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos;
	}
	getById(id: String): DestinoViaje{
		return this.destinos.filter(function(d){return d.id.toString() === id; })[0];
	}
	elegir(d:DestinoViaje) {       //método elegir
	this.destinos.forEach(x => x.setSelected(false));
	d.setSelected (true);
	this.current.next(d);
	}    
	subscribeOnChange(fn) {             //suscribirse a las actualzaciones
		this.current.subscribe(fn);
	}     
}